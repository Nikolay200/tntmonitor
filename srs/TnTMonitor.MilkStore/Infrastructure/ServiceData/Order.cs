﻿using FairMark.OmsApi.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TnTMonitor.MilkStore.Infrastructure.ServiceData
{
    public class Order<TOrderProduct> where TOrderProduct : OrderProduct
    {
        public Order()
        {
            OrderId = Guid.NewGuid();
            Products = new List<TOrderProduct>();
        }
        public Guid OrderId { get; set; }
        public Guid? SuzOrderId { get; set; }
        /// <summary>
        /// Контактное лицо.
        /// </summary>
        public string ContactPerson { get; set; }

        /// <summary>
        /// Способ выпуска товаров в оборот.
        /// </summary>
        public ReleaseMethodTypes ReleaseMethodType { get; set; }

        /// <summary>
        /// Способ изготовления СИ.
        /// </summary>
        public CreateMethodTypes CreateMethodType { get; set; }

        /// <summary>
        /// Идентификатор производственного заказа.
        /// </summary>
        public string ProductionOrderID { get; set; }
        /// <summary>
        /// Идентификатор сервис-провайдера.
        /// </summary>
        public string ServiceProviderID { get; set; }

        public List<TOrderProduct> Products { get; set; }
        public OrderStatus OrderStatus { get; set; }

        public OrderStatuses SuzOrderStatus { get; set; }
        public string SapCode { get; set; }
        public string Batch { get; set; }

        public DateTime? CreateTime { get; set; }
        public string DeclineReason { get; set; }

        public int SetCapacity { get; set; }

        public int GetAggregateCapacity(CisTypes getParrentType)
        {
            if (Products == null) return 0;
            return Products.FirstOrDefault(p => p.CisType == getParrentType)?.Capacity ?? 0;
        }
        public DateTime? FinalaizeTime { get; set; }
    }
}
