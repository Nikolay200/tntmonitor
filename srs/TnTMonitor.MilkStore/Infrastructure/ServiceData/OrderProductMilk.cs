﻿namespace TnTMonitor.MilkStore.Infrastructure.ServiceData
{
    public class OrderProductMilk : OrderProduct
    {
        public string AccompanyingDocument { get; set; }
    }
}
