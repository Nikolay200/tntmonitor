﻿using FairMark.OmsApi.DataContracts;
using PlantX.Tools.DataBase;
using System;
using System.Collections.Generic;

namespace TnTMonitor.MilkStore.Services
{
    public abstract class BaseSuzStoreService<TOrder, TOrderProduct>
        : BaseDataBaseService
        where TOrder : Order<TOrderProduct>, new() where TOrderProduct : OrderProduct, new()

    {
        protected BaseSuzStoreService(string connectionString)
        {
        }

        public abstract void SaveOrder(TOrder updateItem);

        public abstract List<TOrderProduct> GetProductsByOrderId(Guid orderId);

        //public abstract List<OrderInfoView> GetOrdersInfoView();
    }
}