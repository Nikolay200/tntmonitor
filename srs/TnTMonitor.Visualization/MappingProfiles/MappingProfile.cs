﻿using AutoMapper;
using FairMark.OmsApi.DataContracts;
using Newtonsoft.Json;
using PlantX.DataContract.Dto;
using PlantX.DataContract.Infrastructure;
using PlantX.Settings.Infrastructure.ServiceData;
using PlantX.Visualization.Infrastructure;
using TnTMonitor.DataContract.Dto.Aggregation;
using TnTMonitor.DataContract.Dto.Codes;
using TnTMonitor.DataContract.Dto.Order;
using TnTMonitor.DataContract.Dto.Utilisation;
using TnTMonitor.Services.Infrastructure.ServiceData;
using OrderProduct = TnTMonitor.Services.Infrastructure.ServiceData.OrderProduct;

namespace TnTMonitor.Visualization.MappingProfiles
{
    [AutoregisterProfile]
    internal class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            #region Order
            CreateMap<OrderProductMilkDto, UtilisationReportDto_Milk>()
           .ForMember(p => p.UsageType, p => p.MapFrom(m => UsageTypesDto.VERIFIED))
           .ForMember(p => p.Capacity, p => p.MapFrom(m => m.Weight))
           .ForMember(p => p.ExpDate72, p => p.MapFrom(m => m.IsExpDate72 ? JsonConvert.SerializeObject(m.ExpDate72,
               new JsonSerializerSettings { DateFormatString = "yyMMddHHmm" }).Trim('"') : ""))
           .ForMember(p => p.ExpDate, p => p.MapFrom(m => !m.IsExpDate72 ? JsonConvert.SerializeObject(m.ExpDate,
              new JsonSerializerSettings { DateFormatString = "yyMMdd" }).Trim('"') : "")).ReverseMap();

            CreateMap<OrderMilk, OrderMilkDto>().ReverseMap();
            CreateMap<OrderMilk, OrderMilkShortDto>().ReverseMap();
            #endregion

            #region OrderProduct
            CreateMap<OrderProduct, OrderProductDto>().ReverseMap();
            CreateMap<OrderProductMilk, OrderProductMilkDto>().ReverseMap();
            #endregion

            #region Statuses
            CreateMap<BufferStatuses, BufferStatusesDto>().ReverseMap();
            CreateMap<CodeStatus, CodeStatusDto>().ReverseMap();
            CreateMap<OrderStatus, OrderStatusDto>().ReverseMap();
            CreateMap<OrderStatuses, OrderStatusesDto>().ReverseMap();
            CreateMap<OrderStatus, OrderStatusDto>().ReverseMap();
            #endregion

            #region Types
            CreateMap<SettingType, SettingTypeDto> ();
            CreateMap<CisTypes, CisTypesDto>().ReverseMap();            
            //CreateMap<MessageItem, MessageDto>().ReverseMap();
            CreateMap<ReleaseMethodTypes, ReleaseMethodTypesDto>().ReverseMap();
            CreateMap<SerialNumberTypes, SerialNumberTypesDto>().ReverseMap();
            CreateMap<UsageTypes, UsageTypesDto>().ReverseMap();
            CreateMap<CreateMethodTypes, CreateMethodTypesDto>().ReverseMap();
            CreateMap<AggregationTypes, AggregationTypesDto>().ReverseMap();
            #endregion

            #region Reports            
            CreateMap<UtilisationReportDto_Milk, UtilisationReport>().ReverseMap();
            CreateMap<AggregationReport, AggregationReportDto>().ReverseMap();
            CreateMap<DropoutReport, UtilisationReportDto>().ReverseMap();
            #endregion
        }
    }
}
