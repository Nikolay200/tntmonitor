﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PlantX.Visualization.Infrastructure.Menu;
using PlantX.Visualization.ViewModels;

namespace TnTMonitor.Visualization.ModuleInstaller
{
    public class VisualizationModule : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Types.FromThisAssembly().InSameNamespaceAs<MainViewModel>().WithServiceSelf().LifestyleTransient());
            container.Register(Types.FromThisAssembly().BasedOn(typeof(IFirstLevelMenuActions)).WithServiceAllInterfaces().LifestyleSingleton());
            container.Register(Types.FromThisAssembly().BasedOn(typeof(ISecondLevelMenuActions<>)).If(p => !p.IsAbstract && !p.IsInterface).WithServiceAllInterfaces().LifestyleTransient());
            container.Register(Types.FromThisAssembly().BasedOn(typeof(IThirdLevelMenuActions<>)).If(p => !p.IsAbstract && !p.IsInterface).WithServiceAllInterfaces().LifestyleTransient());
        }
    }
}
