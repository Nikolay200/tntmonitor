﻿using PlantX.Visualization.Infrastructure;
using PlantX.Visualization.Infrastructure.Menu;
using PlantX.Visualization.ViewModels;

namespace TnTMonitor.Visualization.MenuActions
{
    public class MainMenuAction : MenuAction<MainViewModel>, IFirstLevelMenuActions
    {
        public MainMenuAction(IScreenFactory<MainViewModel> screenFactory, ISlideCondutor slideCondutor) : base(screenFactory, slideCondutor)
        {
        }

        public int MessageCount { get; }
    }
}
