﻿using Castle.Windsor;
using System;
using System.ServiceProcess;
using TnTMonitor.Services.ModuleInstaller;

namespace TnTMonitor.ServerHost
{
    internal class Program
    {
        public const string ServiceName = "TnTMonitor.Service";

        public class Service : ServiceBase
        {
            public Service()
            {
                ServiceName = Program.ServiceName;
            }

            protected override void OnStart(string[] args)
            {
                Program.Start(args);
            }

            protected override void OnStop()
            {
                Program.Stop();
            }
        }
        static void Main(string[] args)
        {
            if (!Environment.UserInteractive)
                // running as service
                using (var service = new Service())
                    ServiceBase.Run(service);
            else
            {
                // running as console app
                Start(args);
                Console.WriteLine("Press any key to stop...");
                Console.ReadKey(true);
                Stop();
            }
        }

        private static void Start(string[] args)
        {
            var container = new WindsorContainer();
            container.Install(new SuzMilkModule());
        }

        private static void Stop()
        {
        }
    }
}
