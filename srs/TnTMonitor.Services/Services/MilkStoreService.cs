﻿using PlantX.DataContract.Dto;
using PlantX.Tools;
using PlantX.Tools.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using TnTMonitor.DataContract.Dto.Aggregation;
using TnTMonitor.DataContract.Dto.Codes;
using TnTMonitor.DataContract.Dto.Dropout;
using TnTMonitor.DataContract.Dto.Order;
using TnTMonitor.DataContract.Dto.Utilisation;
using TnTMonitor.DataContract.Services;

namespace TnTMonitor.Services.Services
{
    public class MilkStoreService : BaseDataBaseService, IMilkStoreService
    {

        public OrderMilkDto GetOrder(Guid orderId)
        {
            return GetOrder(orderId.ToString());
        }

        public OrderMilkDto GetOrder(string orderId)
        {
            var pars = new[]
            {
                GetDBParameter( "@OrderId", orderId),
            };
            var query = $"SELECT * FROM OrderMilk WHERE OrderId=@OrderId";
            var ret = ReadDataFirstOrDefault<OrderMilkDto>(query, pars);
            if (ret == null) return ret;
            pars = new[]
            {
                GetDBParameter( "@OrderId", orderId),
            };
            ret.Products = ReadData<OrderProductMilkDto>("SELECT * FROM OrderProductMilk WHERE OrderId=@OrderId", pars);
            return ret;
        }
        public void SetOrderStatus(Guid orderId, OrderStatusDto status)
        {
            var pars = new[]
            {
                GetDBParameter( "OrderId", orderId),
                GetDBParameter( "Status", status)
            };
            ExecuteQuery(SetOrderStatusQuery, pars);
        }
        private const string SetOrderStatusQuery = @"UPDATE OrderMilk SET OrderStatus=@Status WHERE OrderId=@OrderId";
        public void SaveOrder(OrderMilkDto newItem)
        {
            var pars = new[]
            {
                GetDBParameter( nameof(newItem.OrderId), newItem.OrderId)                         ,
                GetDBParameter( nameof(newItem.SuzOrderId), newItem.SuzOrderId)                   ,
                GetDBParameter( nameof(newItem.ContactPerson), newItem.ContactPerson)             ,
                GetDBParameter( nameof(newItem.ReleaseMethodType), newItem.ReleaseMethodType)     ,
                GetDBParameter( nameof(newItem.CreateMethodType), newItem.CreateMethodType)       ,
                GetDBParameter( nameof(newItem.ProductionOrderID), newItem.ProductionOrderID)     ,
                GetDBParameter( nameof(newItem.ServiceProviderID), newItem.ServiceProviderID)     ,
                GetDBParameter( nameof(newItem.OrderStatus), newItem.OrderStatus)                 ,
                GetDBParameter( nameof(newItem.SapCode), newItem.SapCode)                         ,
                GetDBParameter( nameof(newItem.Batch), newItem.Batch)                             ,
                GetDBParameter( nameof(newItem.CreateTime), newItem.CreateTime)                   ,
                GetDBParameter( nameof(newItem.DeclineReason), newItem.DeclineReason)             ,
                GetDBParameter( nameof(newItem.FinalaizeTime), newItem.FinalaizeTime)             ,
                GetDBParameter( nameof(newItem.SuzOrderStatus), newItem.SuzOrderStatus)
            };
            ExecuteQuery(
            #region LageQuery
                @"
if exists (select * from OrderMilk where OrderId = @OrderId)
BEGIN
    UPDATE OrderMilk SET 
           ContactPerson     = @ContactPerson
           ,ReleaseMethodType = @ReleaseMethodType
           ,CreateMethodType  = @CreateMethodType
           ,ProductionOrderID = @ProductionOrderID
           ,ServiceProviderID = @ServiceProviderID
           ,OrderStatus       = @OrderStatus
           ,SapCode           = @SapCode
           ,Batch             = @Batch
           ,SuzOrderId        = @SuzOrderId
           ,CreateTime        = @CreateTime
           ,DeclineReason     = @DeclineReason
           ,SuzOrderStatus    = @SuzOrderStatus
           ,FinalaizeTime     = @FinalaizeTime
    WHERE OrderId=@OrderId
END
ELSE
BEGIN
INSERT INTO OrderMilk
           (OrderId
           ,ContactPerson
           ,ReleaseMethodType
           ,CreateMethodType
           ,ProductionOrderID
           ,ServiceProviderID
           ,OrderStatus
           ,SapCode
           ,Batch
           ,SuzOrderId
           ,CreateTime
           ,DeclineReason
           ,SuzOrderStatus
           ,FinalaizeTime)
     VALUES
           (@OrderId
           ,@ContactPerson
           ,@ReleaseMethodType
           ,@CreateMethodType
           ,@ProductionOrderID
           ,@ServiceProviderID
           ,@OrderStatus
           ,@SapCode
           ,@Batch
           ,@SuzOrderId
           ,@CreateTime
           ,@DeclineReason
           ,@SuzOrderStatus
           ,@FinalaizeTime)
END
"
            #endregion
                , pars);
            var existProducts = GetProductsByOrderId(newItem.OrderId);
            existProducts.Synchronizer(newItem.Products, (newProd, storedProd) => newProd.Id == storedProd.Id,
                (newProd, storedProd) => SaveProduct(newProd),
                 SaveProduct, RemoveProduct);
        }
        public void FullRemoveOrder(Guid orderId)
        {
            var pars = new[]
            {
                GetDBParameter( "OrderId", orderId)
            };
            ExecuteQuery(@"
DELETE FROM AggregateItems WHERE OrderId=@OrderId 
DELETE FROM Codes  WHERE OrderId=@OrderId
DELETE FROM OrderProductMilk WHERE OrderId=@OrderId 
DELETE FROM OrderMilk WHERE OrderId=@OrderId 
DELETE FROM OrderProgressItem WHERE OrderId=@OrderId 
DELETE FROM OrdersData WHERE OrderId=@OrderId 
DELETE FROM LineStatus WHERE OrderId=@OrderId", pars);
        }
        private void RemoveProduct(OrderProductMilkDto storedItem)
        {
            var pars = new[]
            {
                GetDBParameter("OrderId", storedItem.OrderId),
            };
            ExecuteQuery("DELETE FROM OrderProductMilk WHERE OrderId=@OrderId", pars);
        }

        private void SaveProduct(OrderProductMilkDto product)
        {
            var pars = new[]
                     {
                    GetDBParameter( nameof(product.OrderId), product.OrderId)                      ,
                    GetDBParameter( nameof(product.Id), product.Id)                                ,
                    GetDBParameter( nameof(product.CisType), product.CisType)                      ,
                    GetDBParameter( nameof(product.ExpDate), product.ExpDate)                      ,
                    GetDBParameter( nameof(product.ExpDate72), product.ExpDate72)                  ,
                    GetDBParameter( nameof(product.ExporterTaxpayerID), product.ExporterTaxpayerID),
                    GetDBParameter( nameof(product.Gtin), product.Gtin)                            ,
                    GetDBParameter( nameof(product.Quantity), product.Quantity)                    ,
                    GetDBParameter( nameof(product.SerialNumberType), product.SerialNumberType)    ,
                    GetDBParameter( nameof(product.TemplateID), product.TemplateID)                ,
                    GetDBParameter( nameof(product.StickerID), product.StickerID)                  ,
                    GetDBParameter( nameof(product.PlannedQuantity), product.PlannedQuantity)      ,
                    GetDBParameter( nameof(product.ProductName), product.ProductName)              ,
                    GetDBParameter( nameof(product.Capacity), product.Capacity)                    ,
                    GetDBParameter( nameof(product.AccompanyingDocument), product.AccompanyingDocument)              ,
                    GetDBParameter( nameof(product.Weight), product.Weight)                        ,
                    GetDBParameter( nameof(product.IsExpDate72), product.IsExpDate72)              ,
                    GetDBParameter( nameof(product.Fat), product.Fat)                              ,
                    GetDBParameter( nameof(product.BufferStatus), product.BufferStatus)            ,
                    GetDBParameter( nameof(product.RejectionReason), product.RejectionReason)
                     };
            ExecuteQuery(
            #region LageQuery
                    @"
if exists (select * from OrderProductMilk where Id = @Id)
BEGIN
    UPDATE OrderProductMilk SET
            CisType           =@CisType
           ,ExpDate           =@ExpDate
           ,ExpDate72         =@ExpDate72
           ,ExporterTaxpayerID=@ExporterTaxpayerID
           ,Gtin              =@Gtin
           ,Quantity          =@Quantity
           ,SerialNumberType  =@SerialNumberType
           ,TemplateID        =@TemplateID
           ,StickerID         =@StickerID
           ,OrderId           =@OrderId
           ,PlannedQuantity   =@PlannedQuantity
           ,ProductName       =@ProductName
           ,Capacity          =@Capacity
           ,AccompanyingDocument       =@AccompanyingDocument
           ,Weight            =@Weight
           ,IsExpDate72       =@IsExpDate72
           ,Fat               =@Fat
           ,BufferStatus      =@BufferStatus
           ,RejectionReason   =@RejectionReason

    WHERE Id=@Id
END
ELSE
BEGIN
    INSERT INTO OrderProductMilk
     (
		    Id
           ,CisType
           ,ExpDate
           ,ExpDate72
           ,ExporterTaxpayerID
           ,Gtin
           ,Quantity
           ,SerialNumberType
           ,TemplateID
           ,StickerID
           ,OrderId
           ,PlannedQuantity
           ,ProductName
           ,Capacity
           ,AccompanyingDocument
           ,Weight 
           ,IsExpDate72
           ,Fat
           ,BufferStatus
           ,RejectionReason
		   )
     VALUES
    (
		    @Id
           ,@CisType
           ,@ExpDate
           ,@ExpDate72
           ,@ExporterTaxpayerID
           ,@Gtin
           ,@Quantity
           ,@SerialNumberType
           ,@TemplateID
           ,@StickerID
           ,@OrderId
           ,@PlannedQuantity
           ,@ProductName
           ,@Capacity
           ,@AccompanyingDocument
           ,@Weight 
           ,@IsExpDate72
           ,@Fat
           ,@BufferStatus
           ,@RejectionReason
		   )
END
"
            #endregion
                    , pars);
        }

        public List<OrderProductMilkDto> GetProductsByOrderId(Guid orderId)
        {
            var pars = new[]
            {
                GetDBParameter("OrderId", orderId),
            };
            var res = ReadData<OrderProductMilkDto>("SELECT * FROM OrderProductMilk WHERE OrderId=@OrderId", pars);
            return res;
        }

        public List<OrderMilkDto> GetAllOrders()
        {
            return ReadData<OrderMilkDto>("SELECT * FROM OrderMilk");
        }


        public string SaveCodes(List<CodeItemDto> items)
        {
            var res = new List<string>();
            foreach (var item in items)
            {
                var pars = new[]
                {
                    GetDBParameter( nameof(item.OrderId), item.OrderId),
                    GetDBParameter( nameof(item.Code), item.Code),
                    GetDBParameter( nameof(item.OrderProductId), item.OrderProductId),
                    GetDBParameter( nameof(item.UseUntil), item.UseUntil),
                    GetDBParameter( nameof(item.CodeType), (int) item.CodeType),
                    GetDBParameter( nameof(item.CodeStatus), (int) item.CodeStatus),
                    GetDBParameter( nameof(item.IsDefect), item.IsDefect),
                };
                var s = ReadData<StringValueWrapper>(InsertUpdateCodes, pars).FirstOrDefault()?.Value;
                if (!string.IsNullOrEmpty(s)) res.Add(s);
            }

            return string.Join(" ", res);
        }

        private const string InsertUpdateCodes = @"

if exists (select * from Codes where Code = @Code)
begin
    SELECT @Code AS Value
end
else
begin
INSERT INTO Codes
           (
		    OrderId
           ,Code
           ,UseUntil
           ,CodeType
           ,CodeStatus
           ,IsDefect
           ,OrderProductId
)
     VALUES
           (
		    @OrderId
           ,@Code
           ,@UseUntil
           ,@CodeType
           ,@CodeStatus
           ,@IsDefect
           ,@OrderProductId
)   
SELECT '' AS Value
end
";



        public List<UtilisationReportDto_Milk> GetUtilisationReportData(Guid orderId)
        {
            var ret = new List<UtilisationReportDto_Milk>();
            var items = GetProductsByOrderId(orderId);
            foreach (var item in items)
            {

                var newItem = item.PlantXMap<OrderProductMilkDto, UtilisationReportDto_Milk>();

                var pars = new[]
                {
                    GetDBParameter("@OrderProductId", item.Id),
                    GetDBParameter("@CodeStatus", CodeStatusDto.Validated)
                };
                var sntinsQuery = "SELECT Code Value FROM Codes WHERE OrderProductId=@OrderProductId AND CodeStatus = @CodeStatus";
                newItem.Sntins = ReadData<StringValueWrapper>(sntinsQuery, pars).Select(p => p.Value).ToList();
                ret.Add(newItem);
            }
            return ret;
        }
        public List<SettingTypeDto> GetSettings()
        {
            var existSettings = ReadData<SettingTypeDto>(
                "SELECT * FROM SystemSettings");

            foreach (var item in existSettings)
            {
                var e = existSettings.FirstOrDefault(p => p.Id == item.Id);
                if (e == null)
                {
                    existSettings.Add(item);
                }
            }
            return existSettings;
        }
        private List<AggregateItemDto> GetAggregateItemsByOrder(Guid orderId)
        {
            var pars = new[]
            {
                GetDBParameter("@OrderId", orderId)
            };
            var ret = ReadData<AggregateItemDto>("SELECT * FROM AggregateItems WHERE OrderId=@OrderId", pars);
            return ret;
        }
        public List<AggregationReportDto> GetAggregationReportData(Guid orderId)
        {
            var ret = new AggregationReportDto();

            var participantId = GetSettings().FirstOrDefault(p => p.SystemName == "ParticipantId");
            ret.ParticipantId = participantId.SystemName;
            var aggregateItems = GetAggregateItemsByOrder(orderId);
            ret.AggregationUnits = new List<AggregationUnitDto>();
            foreach (var pallet in aggregateItems.Where(p => p.AggregateType == CisTypesDto.SET))
            {
                ret.AggregationUnits.Add(GetAggregationUnit(pallet.AggregatedCount,
                        AggregationTypesDto.AGGREGATION,
                        pallet.Capacity,
                        pallet.Code,
                        aggregateItems
                        .Where(p => p.ParentAggregateItem == pallet.Id && p.AggregateType != CisTypesDto.SET)
                        .Select(p => p.Code).ToList()
                ));
            }

            var pars = new[]
            {
                GetDBParameter("@OrderId", orderId),
                GetDBParameter("@CodeType", CisTypesDto.UNIT),
                GetDBParameter("@CodeStatus", CodeStatusDto.Validated),
            };
            var codes = ReadData<CodeItemDto>("SELECT * FROM Codes WHERE OrderId=@OrderId AND CodeType=@CodeType AND CodeStatus=@CodeStatus", pars);
            foreach (var box in aggregateItems.Where(p => p.AggregateType == CisTypesDto.GROUP))
            {
                ret.AggregationUnits.Add(GetAggregationUnit(
                        box.AggregatedCount,
                        AggregationTypesDto.AGGREGATION,
                        box.Capacity,
                        box.Code,
                        codes.Where(p => p.PackItem == box.Id)
                        .Select(p => p.Code).ToList()
                ));
            }
            return new List<AggregationReportDto> { ret };
        }
        AggregationUnitDto GetAggregationUnit(int aggregatedCount, AggregationTypesDto aggregationType, int capacity
            , string code, List<string> sntins)
        {
            return new AggregationUnitDto
            {
                AggregatedItemsCount = aggregatedCount,
                AggregationType = aggregationType,
                AggregationUnitCapacity = capacity,
                UnitSerialNumber = code,
                Sntins = sntins
            };
        }
        public List<DropoutReportDto> GetDropoutReportData(Guid orderId)
        {
            var ret = new List<DropoutReportDto>();
            var items = GetProductsByOrderId(orderId);
            foreach (var item in items)
            {
                var newItem = new DropoutReportDto
                {
                    DropoutReason = DropoutReasonsDto.DEFECT,
                };
                var pars = new[]
                {
                    GetDBParameter("@OrderProductId", item.Id),
                    GetDBParameter("@IsDefect", true)
                };
                var sntinsQuery = "SELECT Code Value FROM Codes WHERE OrderProductId=@OrderProductId AND IsDefect = @IsDefect";
                newItem.Sntins = ReadData<StringValueWrapper>(sntinsQuery, pars).Select(p => p.Value).ToList();
                ret.Add(newItem);
            }
            return ret;
        }
    }
    class StringValueWrapper
    {
        public string Value { get; set; }
    }
}
