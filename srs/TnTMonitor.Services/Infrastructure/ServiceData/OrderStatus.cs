﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace TnTMonitor.Services.Infrastructure.ServiceData
{
    public enum OrderStatus
    {
        /// <summary>
        /// Заказ создан в базе данных.
        /// </summary>
        [Description("Черновик")]
        [JsonProperty("DRAFT")]
        Draft = 0,

        /// <summary>
        /// КМ для заказа получены.
        /// </summary>
        [Description("Готов к запуску")]
        [JsonProperty("Ready")]
        Ready = 1,

        /// <summary>
        /// Заказ стартован.
        /// </summary>
        [Description("Стартован")]
        [JsonProperty("STARTED")]
        Started = 2,

        /// <summary>
        /// Заказ остановлен.
        /// </summary>
        [Description("Остановлен")]
        [JsonProperty("STOPPED")]
        Stopped = 3,

        /// <summary>
        /// Заказ завершён.
        /// </summary>
        [Description("Завершен")]
        [JsonProperty("FINALIZED")]
        Finalized = 4,

        /// <summary>
        /// Заказ создаётся в СУЗ.
        /// </summary>
        [Description("Создание в СУЗ")]
        [JsonProperty("CreatedSuz")]
        CreatedSuz = 5,

        /// <summary>
        /// Регистрация в СУЗ.
        /// </summary>
        [Description("Регистрация в СУЗ")]
        [JsonProperty("PendingSuz")]
        PendingSuz = 6,

        /// <summary>
        /// Зарегистрирован в СУЗ.
        /// </summary>
        [Description("Зарегистрирован в СУЗ")]
        [JsonProperty("ApprovedSuz")]
        ApprovedSuz = 7,

        /// <summary>
        /// Доступен в СУЗ.
        /// </summary>
        [Description("Доступен в СУЗ")]
        [JsonProperty("EnabledSuz")]
        EnabledSuz = 8,

        /// <summary>
        /// Закрыт в СУЗ.
        /// </summary>
        [Description("Закрыт в СУЗ")]
        [JsonProperty("ClosedSuz")]
        ClosedSuz = 9,

        /// <summary>
        /// Аннулирован в СУЗ.
        /// </summary>
        [Description("Аннулирован в СУЗ")]
        [JsonProperty("ExpiredSuz")]
        ExpiredSuz = 10,

        /// <summary>
        /// Не подтверждён в СУЗ.
        /// </summary>
        [Description("Не подтверждён в СУЗ")]
        [JsonProperty("DeclinedSuz")]
        DeclinedSuz = 11,

        /// <summary>
        /// Ошибка при создании заказа.
        /// </summary>
        [Description("Ошибка при создании заказа")]
        [JsonProperty("OrderCreateError")]
        OrderCreateError = 12,

        /// <summary>
        /// Буфер кодов создаётся в СУЗ.
        /// </summary>
        [Description("Буфер кодов создаётся в СУЗ")]
        [JsonProperty("ActiveCodesSuz")]
        ActiveCodesSuz = 13,

        /// <summary>
        /// Буфер кодов закрыт в СУЗ.
        /// </summary>
        [Description("Коды закрыты")]
        [JsonProperty("ClosedCodesSuz")]
        ClosedCodesSuz = 14,

        /// <summary>
        /// Буфер кодов исчерпан в СУЗ.
        /// </summary>
        [Description("Коды исчерпаны")]
        [JsonProperty("ExhausedCodesSuz")]
        ExhausedCodesSuz = 15,

        /// <summary>
        /// Буфер аннулирован (по истечению срока годности не использованных КМ).
        /// </summary>
        [Description("Коды аннулированы")]
        [JsonProperty("ExpiredCodesSuz")]
        ExpiredCodesSuz = 16,

        /// <summary>
        /// Буфер КМ находится в ожидании.
        /// </summary>
        [Description("Коды не загружаются")]
        [JsonProperty("PendingCodesSuz")]
        PendingCodesSuz = 17,

        /// <summary>
        /// Буфер более не доступен для работы.
        /// </summary>
        [Description("Коды недоступны")]
        [JsonProperty("RejectedCodesSuz")]
        RejectedCodesSuz = 18,

        /// <summary>
        /// Ошибка при загрузке кодов.
        /// </summary>
        [Description("Ошибка при загрузке кодов")]
        [JsonProperty("CodeLoadError")]
        CodeLoadError = 19,

        /// <summary>
        /// Отправка отчётов в СУЗ.
        /// </summary>
        [Description("Отправка отчётов")]
        [JsonProperty("SendingReports")]
        SendingReports = 20,

        /// <summary>
        /// Отчёты отправлены в СУЗ.
        /// </summary>
        [Description("Отчёты отправлены")]
        [JsonProperty("ReportsDelivered")]
        ReportsDelivered = 21,

        /// <summary>
        /// Ошибка при отправке отчётов.
        /// </summary>
        [Description("Ошибка при отправке отчётов")]
        [JsonProperty("ReportSendingError")]
        ReportSendingError = 22
    }
}
