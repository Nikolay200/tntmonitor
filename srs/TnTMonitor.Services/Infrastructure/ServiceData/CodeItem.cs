﻿using FairMark.OmsApi.DataContracts;
using System;

namespace TnTMonitor.Services.Infrastructure.ServiceData
{
    public class CodeItem
    {
        public long Id { get; set; }
        public Guid OrderId { get; set; }
        public Guid OrderProductId { get; set; }
        public string Code { get; set; }
        public DateTime? UseUntil { get; set; }
        public CisTypes CodeType { get; set; }
        public CodeStatus CodeStatus { get; set; }
        public bool IsDefect { get; set; }
        public Guid? ClasterId { get; set; }
        public Guid? AggregateItem { get; set; }
        public Guid? PackItem { get; set; }
        public DateTime? PrintedTime { get; set; }
        //public SeparatePrinteredStatus SeparatePrinteredStatus { get; set; }
        /// <summary>Identifier of code block (Идентификатор блока кодов)</summary>
        public string BlockID { get; set; }
    }

    public enum SeparatePrinteredStatus
    {
        Unused,
        Printed
    }

    public class CodeItemShort
    {
        public long Id { get; set; }
        public string Code { get; set; }
    }
}
