﻿namespace TnTMonitor.Services.Infrastructure.ServiceData
{
    public class OrderProductMilk : OrderProduct
    {
        public string AccompanyingDocument { get; set; }
    }
}
