﻿using PlantX.DataContract.Dto;
using PlantX.DataContract.Infrastructure;
using System;
using System.Collections.Generic;

namespace TnTMonitor.Services.Infrastructure
{
    public class ModuleSettings : IModuleSettings
    {
        public List<SettingTypeDto> InitTypes()
        {
            return new List<SettingTypeDto>
            {
                new SettingTypeDto
                {
                    Id = new Guid("d90f3c27-122d-4128-b36d-45e87e72a6ed"),
                    SystemName = "TrueApiServer",
                    DefaultDescription = "Адрес сервера аутентификации",
                    Group = "СУЗ"
                },
                new SettingTypeDto
                {
                    Id = new Guid("591c13bb-bef7-4032-88fc-52f199895630"),
                    SystemName = "SuzServer",
                    DefaultDescription = "Адрес сервера СУЗ",
                    Group = "СУЗ"
                },
                new SettingTypeDto
                {
                    Id = new Guid("0f68bb1f-3430-4e84-a561-82bddbd5f848"),
                    SystemName = "CertificateThumbprint",
                    DefaultDescription = "Отпечаток сертификата",
                    Group = "СУЗ"
                },
                new SettingTypeDto
                {
                    Id = new Guid("aa2f468d-af18-47f3-a30c-3cb71d062ac0"),
                    SystemName = "OmsID",
                    DefaultDescription = "Идентификатор пользователя",
                    Group = "СУЗ"
                },
                new SettingTypeDto
                {
                    Id = new Guid("4e2f6a63-e606-433b-bf81-284e7b1f29bb"),
                    SystemName = "ConnectionID",
                    DefaultDescription = "Идентификатор устройства",
                    Group = "СУЗ"
                },
                new SettingTypeDto
                {
                    Id = new Guid("43f6eaf2-d312-46cc-bb3a-b46d957c41a9"),
                    SystemName = "ParticipantID",
                    DefaultDescription = "Идентификационный номер(ИНН) налогоплательщика",
                    Group = "СУЗ"
                },
                new SettingTypeDto
                {
                    Id = new Guid("d6e83791-197c-4075-b583-0869acc817cc"),
                    SystemName = "IterationsCount",
                    DefaultValue = "50",
                    DefaultDescription = "Количество запросов к СУЗ для подтверждения статуса заказа",
                    Group = "СУЗ"
                },
                new SettingTypeDto
                {
                    Id = new Guid("67357815-cbb4-42e8-a023-ad896b433ef8"),
                    SystemName = "DelayMilliseconds",
                    DefaultValue = "500",
                    DefaultDescription = "Время ожидания(мс) между запросами к СУЗ",
                    Group = "СУЗ"
                },
                new SettingTypeDto
                {
                    Id = new Guid("0d8adc54-f032-44e3-8258-dcd7f51d6ab4"),
                    SystemName = "CodesBlockQuantity",
                    DefaultValue = "1000",
                    DefaultDescription = "Количество кодов в блоке",
                    Group = "СУЗ"
                },
            };
        }
    }
}
