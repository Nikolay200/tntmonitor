﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using TnTMonitor.DataContract.Services;
using TnTMonitor.Services.Services;

namespace TnTMonitor.Services.ModuleInstaller
{
    public class SuzMilkModule : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ISuzApiService>()
                .ImplementedBy(typeof(SuzApiService))
                .LifestyleSingleton());
            container.Register(Component.For<IMilkStoreService>()
                .ImplementedBy(typeof(MilkStoreService))
                .LifestyleSingleton());
        }
    }
}
