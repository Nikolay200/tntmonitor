﻿using System.ComponentModel;

namespace TnTMonitor.DataContract.Dto.Order
{
    public enum OrderStatusDto
    {
        /// <summary>
        /// Заказ создан в базе данных.
        /// </summary>
        [Description("Черновик")]
        Draft = 0,
        /// <summary>
        /// КМ для заказа получены.
        /// </summary>
        [Description("Готов к запуску")]
        Ready = 1,
        /// <summary>
        /// Заказ стартован.
        /// </summary>
        [Description("Стартован")]
        Started = 2,
        /// <summary>
        /// Заказ остановлен.
        /// </summary>
        [Description("Остановлен")]
        Stopped = 3,
        /// <summary>
        /// Заказ завершён.
        /// </summary>
        [Description("Завершен")]
        Finalized = 4,
        /// <summary>
        /// Заказ создаётся в СУЗ.
        /// </summary>
        [Description("Создание в СУЗ")]
        CreatedSuz = 5,
        /// <summary>
        /// Регистрация в СУЗ.
        /// </summary>
        [Description("Регистрация в СУЗ")]
        PendingSuz = 6,
        /// <summary>
        /// Зарегистрирован в СУЗ.
        /// </summary>
        [Description("Зарегистрирован в СУЗ")]
        ApprovedSuz = 7,
        /// <summary>
        /// Доступен в СУЗ.
        /// </summary>
        [Description("Доступен в СУЗ")]
        EnabledSuz = 8,
        /// <summary>
        /// Закрыт в СУЗ.
        /// </summary>
        [Description("Закрыт в СУЗ")]
        ClosedSuz = 9,
        /// <summary>
        /// Аннулирован в СУЗ.
        /// </summary>
        [Description("Аннулирован в СУЗ")]
        ExpiredSuz = 10,
        /// <summary>
        /// Не подтверждён в СУЗ.
        /// </summary>
        [Description("Не подтверждён в СУЗ")]
        DeclinedSuz = 11,
        /// <summary>
        /// Ошибка при создании заказа.
        /// </summary>
        [Description("Ошибка при создании заказа")]
        OrderCreateError = 12,
        /// <summary>
        /// Буфер кодов создаётся в СУЗ.
        /// </summary>
        [Description("Буфер кодов создаётся в СУЗ")]
        ActiveCodesSuz = 13,
        /// <summary>
        /// Буфер кодов закрыт в СУЗ.
        /// </summary>
        [Description("Коды закрыты")]
        ClosedCodesSuz = 14,
        /// <summary>
        /// Буфер кодов исчерпан в СУЗ.
        /// </summary>
        [Description("Коды исчерпаны")]
        ExhausedCodesSuz = 15,
        /// <summary>
        /// Буфер аннулирован (по истечению срока годности не использованных КМ).
        /// </summary>
        [Description("Коды аннулированы")]
        ExpiredCodesSuz = 16,
        /// <summary>
        /// Буфер КМ находится в ожидании.
        /// </summary>
        [Description("Коды не загружаются")]
        PendingCodesSuz = 17,
        /// <summary>
        /// Буфер более не доступен для работы.
        /// </summary>
        [Description("Коды недоступны")]
        RejectedCodesSuz = 18,
        /// <summary>
        /// Ошибка при загрузке кодов.
        /// </summary>
        [Description("Ошибка при загрузке кодов")]
        CodeLoadError = 19,
        /// <summary>
        /// Отправка отчётов в СУЗ.
        /// </summary>
        [Description("Отправка отчётов")]
        SendingReports = 20,
        /// <summary>
        /// Отчёты отправлены в СУЗ.
        /// </summary>
        [Description("Отчёты отправлены")]
        ReportsDelivered = 21,
        /// <summary>
        /// Ошибка при отправке отчётов.
        /// </summary>
        [Description("Ошибка при отправке отчётов")]
        ReportSendingError = 22
    }
}
