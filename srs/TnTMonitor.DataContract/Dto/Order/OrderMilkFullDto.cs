﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TnTMonitor.DataContract.Dto.Order
{
    public class OrderMilkDto
    {
        public Guid OrderId { get; set; }
        public Guid? SuzOrderId { get; set; }
        /// <summary>
        /// Контактное лицо.
        /// </summary>
        public string ContactPerson { get; set; }

        /// <summary>
        /// Способ выпуска товаров в оборот.
        /// </summary>
        public ReleaseMethodTypesDto ReleaseMethodType { get; set; }

        /// <summary>
        /// Способ изготовления СИ.
        /// </summary>
        public CreateMethodTypesDto CreateMethodType { get; set; }

        /// <summary>
        /// Идентификатор производственного заказа.
        /// </summary>
        public string ProductionOrderID { get; set; }
        /// <summary>
        /// Идентификатор сервис-провайдера.
        /// </summary>
        public string ServiceProviderID { get; set; }

        public List<OrderProductMilkDto> Products { get; set; }
        public OrderStatusDto OrderStatus { get; set; }

        public OrderStatusesDto SuzOrderStatus { get; set; }
        public string SapCode { get; set; }
        public string Batch { get; set; }

        public DateTime? CreateTime { get; set; }
        public string DeclineReason { get; set; }

        public int SetCapacity { get; set; }

        public DateTime? FinalaizeTime { get; set; }
    }
}
