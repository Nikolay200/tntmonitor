﻿using System;

namespace TnTMonitor.DataContract.Dto.Order
{
    public class OrderMilkShortDto
    {
        public Guid OrderId { get; set; }
        /// <summary>
        /// Идентификатор производственного заказа.
        /// </summary>
        public string ProductionOrderID { get; set; }
        /// <summary>
        /// Список названий продуктов
        /// </summary>
        public string ProductsName { get; set; }
        /// <summary>
        /// Состояние заказа
        /// </summary>
        public string OrderStatus { get; set; }
        public string ProductsString { get; set; }
    }
}
