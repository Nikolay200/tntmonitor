﻿namespace TnTMonitor.DataContract.Dto.Order
{
    public class OrderProductMilkDto : OrderProductDto
    {
        public string AccompanyingDocument { get; set; }
    }
}
