﻿using System;
using TnTMonitor.DataContract.Dto.Codes;

namespace TnTMonitor.DataContract.Dto.Order
{
    public class OrderProductDto
    {
        public OrderProductDto()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public Guid OrderId { get; set; }

        /// <summary>CIS type (Тип КМ)</summary>
        public CisTypesDto CisType { get; set; }

        /// <summary>
        /// Expiry date of the product (expiration date more than 72 hours)
        /// Дата окончания срока годности продукции (срок хранения более 72 часов)
        /// </summary>
        public DateTime? ExpDate { get; set; }

        /// <summary>Expiration date of the product (shelf life less than 72 hours)
        /// Дата окончания срока годности продукции (срок хранения менее 72 часов)
        /// </summary>
        public DateTime? ExpDate72 { get; set; }

        /// <summary>ИНН/УНБ (или аналог) экспортера</summary>
        /// <remarks>
        /// Примечание*: Поле «exporterTaxpayerId» становится обязательным, если в поле
        /// releaseMethodType (способ выпуска товара в оборот) было выбрано значение
        /// «CROSSBORDER» (Ввезен в РФ из стран ЕАЭС).
        /// </remarks>
        public string ExporterTaxpayerID { get; set; }

        /// <summary>
        /// Код товара (GTIN).
        /// </summary>
        public string Gtin { get; set; }

        /// <summary>
        /// Количество КМ.
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Способ генерации серийных номеров.
        /// Справочное значение «Способ формирования индивидуального
        /// серийного номера», см. раздел 5.3.1.2
        /// </summary>
        /// <remarks>
        /// Примечание: для товарной группы «Табачная продукция» первично установленная
        /// схема генерации и структура шаблона КМ для конкретного типа товара(GTIN),
        /// определяемая атрибутом «serialNumberType», не может быть изменена в дальнейшем.
        /// </remarks>
        public SerialNumberTypesDto SerialNumberType { get; set; }

        /// <summary>
        /// Идентификатор шаблона КМ. Справочное значение
        /// «Способ изготовления», см.раздел 5.3.1.4
        /// </summary>
        /// <remarks>
        /// Для шаблона молочной продукции templateId=20 при
        /// самостоятельном способе генерации длина серийных номеров должна быть равна 5-ти
        /// символам. При эмиссии кодов маркировки серийный номер будет состоять из 6 символов,
        /// включая код страны. Код страны проставляется Сервером эмиссии и указывается перед
        /// полученным серийным номером (см.раздел 5.3.1.13).
        /// </remarks>
        public int TemplateID { get; set; }

        /// <summary>
        /// Идентификатор этикетки.
        /// </summary>
        /// <remarks>
        /// Примечание: поле «stickerId» заполняется при создании
        /// заказа в рамках процесса дистрибуции.
        /// В документации противоречие: тип указан как String,
        /// а в примерах передается значение Integer.
        /// Сделал тип Integer, по аналогии с полем TemplateID.
        /// </remarks>
        public int StickerID { get; set; }

        public int PlannedQuantity { get; set; }
        public string ProductName { get; set; }
        public int? Capacity { get; set; }
        public string RejectionReason { get; set; }
        public BufferStatusesDto BufferStatus { get; set; }

        public double Weight { get; set; }
        public double Fat { get; set; }
        public bool IsExpDate72 { get; set; }
    }
}
