﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TnTMonitor.DataContract.Dto.Codes;

namespace TnTMonitor.DataContract.Dto.Order
{
    public class OrderDto<TOrderProductDto> where TOrderProductDto : OrderProductDto
    {
        public OrderDto()
        {
            OrderId = Guid.NewGuid();
            Products = new List<TOrderProductDto>();
        }
        public Guid OrderId { get; set; }
        public Guid? SuzOrderId { get; set; }
        /// <summary>
        /// Контактное лицо.
        /// </summary>
        public string ContactPerson { get; set; }

        /// <summary>
        /// Способ выпуска товаров в оборот.
        /// </summary>
        public ReleaseMethodTypesDto ReleaseMethodType { get; set; }

        /// <summary>
        /// Способ изготовления СИ.
        /// </summary>
        public CreateMethodTypesDto CreateMethodType { get; set; }

        /// <summary>
        /// Идентификатор производственного заказа.
        /// </summary>
        public string ProductionOrderID { get; set; }
        /// <summary>
        /// Идентификатор сервис-провайдера.
        /// </summary>
        public string ServiceProviderID { get; set; }

        public List<TOrderProductDto> Products { get; set; }
        public OrderStatusDto OrderStatus { get; set; }

        public OrderStatusesDto SuzOrderStatus { get; set; }
        public string SapCode { get; set; }
        public string Batch { get; set; }

        public DateTime? CreateTime { get; set; }
        public string DeclineReason { get; set; }

        public int SetCapacity { get; set; }

        public int GetAggregateCapacity(CisTypesDto getParrentType)
        {
            if (Products == null) return 0;
            return Products.FirstOrDefault(p => p.CisType == getParrentType)?.Capacity ?? 0;
        }
        public DateTime? FinalaizeTime { get; set; }
    }
}
