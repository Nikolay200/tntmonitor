﻿namespace TnTMonitor.DataContract.Dto.Logging
{
    public enum LogTypeDto
    {
        Info,
        Warring,
        Critical
    }
}
