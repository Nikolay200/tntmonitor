﻿using System;

namespace TnTMonitor.DataContract.Dto.Logging
{
    public class LogItemDto
    {
        public DateTime CreateTime { get; set; }
        public string Text { get; set; }
        public string Source { get; set; }
        public string LogType { get; set; }
    }
}
