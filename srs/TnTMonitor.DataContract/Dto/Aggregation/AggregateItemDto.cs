﻿using System;
using TnTMonitor.DataContract.Dto.Codes;

namespace TnTMonitor.DataContract.Dto.Aggregation
{
    public class AggregateItemDto
    {
        public Guid Id { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsSended { get; set; }
        public string Code { get; set; }
        public int Capacity { get; set; }
        public Guid OrderId { get; set; }
        public Guid OrderProductId { get; set; }
        public bool IsFull { get; set; }
        public bool IsAggregated { get; set; }
        public CisTypesDto AggregateType { get; set; }
        public Guid? ParentAggregateItem { get; set; }
        public int AggregatedCount { get; set; }
    }
}
