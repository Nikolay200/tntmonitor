﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TnTMonitor.DataContract.Dto.Aggregation
{
    /// <summary>
    /// 4.5.3. Метод «Отправить отчёт об агрегации КМ»
    /// </summary>
    [DataContract]
    public class AggregationReportDto
    {
        /// <summary>
        /// Идентификационный номер налогоплательщика
        /// </summary>
        [DataMember(Name = "participantId")]
        public string ParticipantId { get; set; }

        /// <summary>
        /// Массив единиц агрегации
        /// </summary>
        [DataMember(Name = "aggregationUnits")]
        public List<AggregationUnitDto> AggregationUnits { get; set; }
    }
}
