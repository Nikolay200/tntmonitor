﻿namespace TnTMonitor.DataContract.Dto.Message
{
    public enum MessageTypeDto
    {
        Undefined,
        Info,
        Warring,
        Alarm,
        Critical
    }
}
