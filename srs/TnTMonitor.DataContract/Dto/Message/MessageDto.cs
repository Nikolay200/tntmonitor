﻿using System;

namespace TnTMonitor.DataContract.Dto.Message
{
    public class MessageDto
    {
        public MessageDto()
        {
            CreateTime = DateTime.Now;
        }
        public DateTime CreateTime { get; set; }
        public string Text { get; set; }
        public string MessageGroup { get; set; }
        public MessageTypeDto MessageType { get; set; }
    }
}
