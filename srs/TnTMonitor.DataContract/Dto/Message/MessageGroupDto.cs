﻿using System.ComponentModel;

namespace TnTMonitor.DataContract.Dto.Message
{
    public enum MessageGroupDto
    {
        [Description("Не определено")]
        Undefine,
        [Description("Валидация кодов")]
        CodeValidate,
        [Description("L3")]
        L3,
        [Description("Авторизация")]
        Auth,
        [Description("Устройства")]
        Device,
        [Description("База данных")]
        SQL,
        [Description("Агрегация")]
        CodeAggregate
    }
}
