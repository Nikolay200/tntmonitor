﻿using System;

namespace TnTMonitor.DataContract.Dto.Settings
{
    public class SettingValueDto
    {
        public Guid TypeId { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
    }
}
