﻿using System;

namespace TnTMonitor.DataContract.Dto.Settings
{
    public class SettingTypeDto
    {
        public Guid Id { get; set; }
        public string SystemName { get; set; }
        public string DefaultDescription { get; set; }
        public string DefaultValue { get; set; }
        public string Group { get; set; }
    }
}
