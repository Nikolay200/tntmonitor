﻿using System;

namespace TnTMonitor.DataContract.Dto.Codes
{
    public class CodeItemDto
    {
        public long Id { get; set; }
        public Guid OrderId { get; set; }
        public Guid OrderProductId { get; set; }
        public string Code { get; set; }
        public DateTime? UseUntil { get; set; }
        public CisTypesDto CodeType { get; set; }
        public CodeStatusDto CodeStatus { get; set; }
        public bool IsDefect { get; set; }
        public Guid? ClasterId { get; set; }
        public Guid? AggregateItem { get; set; }
        public Guid? PackItem { get; set; }
        public DateTime? PrintedTime { get; set; }
        //public SeparatePrinteredStatus SeparatePrinteredStatus { get; set; }
        /// <summary>Identifier of code block (Идентификатор блока кодов)</summary>
        public string BlockID { get; set; }
    }

    public enum SeparatePrinteredStatus
    {
        Unused,
        Printed
    }

    public class CodeItemShort
    {
        public long Id { get; set; }
        public string Code { get; set; }
    }
}
