﻿using System.ComponentModel;

namespace TnTMonitor.DataContract.Dto.Codes
{
    public enum CodeStatusDto
    {
        [Description("Код не использовался")]
        New,
        [Description("Код находится в буфере принтера")]
        Printing,
        [Description("Код напечатан")]
        Printed,
        [Description("Код валидирован")]
        Validated,
    }
}
