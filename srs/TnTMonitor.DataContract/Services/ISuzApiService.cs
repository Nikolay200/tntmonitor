﻿using System;

namespace TnTMonitor.DataContract.Services
{
    public interface ISuzApiService
    {
        /// <summary>
        /// Initialisation of parameters
        /// Инициализация параметров
        /// </summary>
        void Init();

        /// <summary>
        /// Create Order.
        /// Создание заказа в СУЗ.
        /// </summary>
        void CreateOrder(Guid orderId);

        /// <summary>
        /// Load Codes.
        /// Загрузить КМ в базу данных из СУЗ.
        /// </summary>
        void LoadCodes(Guid orderId);

        /// <summary>
        /// Send Reports.
        /// Отправка отчётов в СУЗ (отчёт об использовании, отчёт об агрегации и отчёт о выбытии/отбраковке КМ).
        /// </summary>
        void SendReports(Guid orderId);

        /// <summary>
        /// Synchronization.
        /// Синхронизация с заказами в СУЗ.
        /// </summary>
        void SynchronizeWithSuz();
    }
}
