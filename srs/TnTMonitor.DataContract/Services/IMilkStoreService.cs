﻿using System;
using System.Collections.Generic;
using TnTMonitor.DataContract.Dto.Aggregation;
using TnTMonitor.DataContract.Dto.Codes;
using TnTMonitor.DataContract.Dto.Dropout;
using TnTMonitor.DataContract.Dto.Order;
using TnTMonitor.DataContract.Dto.Utilisation;

namespace TnTMonitor.DataContract.Services
{
    public interface IMilkStoreService
    {
        void SaveOrder(OrderMilkDto newItem);
        List<OrderMilkDto> GetAllOrders();
        OrderMilkDto GetOrder(Guid orderId);
        void SetOrderStatus(Guid orderId, OrderStatusDto status);
        string SaveCodes(List<CodeItemDto> items);
        void FullRemoveOrder(Guid orderId);
        List<UtilisationReportDto_Milk> GetUtilisationReportData(Guid orderId);
        List<AggregationReportDto> GetAggregationReportData(Guid orderId);
        List<DropoutReportDto> GetDropoutReportData(Guid orderId);
        List<OrderProductMilkDto> GetProductsByOrderId(Guid orderId);       
    }
}
