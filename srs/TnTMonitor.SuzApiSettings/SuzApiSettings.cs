﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TnTMonitor.DataContract.Services;

namespace TnTMonitor.SuzApiSettings
{
    public class SuzApiSettings : ISuzApiSettings
    {
        private readonly IStoreService _storeService;
        private string _trueApiServer;
        private string _suzServer;
        private string _crtificateThumbprint;
        private string _connectionID;
        private string _omsId;
        private int _codesBlockQuantity;
        private int _delayMilliseconds;
        private int _iterationsCount;
        private string _participantId;
        public SuzApiSettings(
            IStoreService storeService)
        {
            _storeService = storeService;
            Refrsh();
        }
        public string TrueApiServer()
        {
            return _trueApiServer;
        }

        public string SuzServer()
        {
            return _suzServer;
        }
        public string CertificateThumbprint()
        {
            return _crtificateThumbprint;
        }
        public string OmsId()
        {
            return _omsId;
        }
        public string ConnectionId()
        {
            return _connectionID;
        }
        public int CodesBlockQuantity()
        {
            return _codesBlockQuantity;
        }
        public int DelayMilliseconds()
        {
            return _delayMilliseconds;
        }
        public int IterationsCount()
        {
            return _iterationsCount;
        }
        public string ParticipantId()
        {
            return _participantId;
        }

        public bool IsSettingsConfigured()
        {
            return !string.IsNullOrEmpty(_trueApiServer)
                   && !string.IsNullOrEmpty(_suzServer)
                   && !string.IsNullOrEmpty(_omsId)
                   && !string.IsNullOrEmpty(_connectionID);
        }


        public void Refrsh()
        {
            var settings = _storeService.GetSettings();
            _trueApiServer = settings
                .FirstOrDefault(p =>
                    p.SystemType == SettingSystemType.TrueApiServer)?.Value;
            _suzServer = settings
                .FirstOrDefault(p =>
                    p.SystemType == SettingSystemType.SuzServer)?.Value;
            _omsId = settings
                .FirstOrDefault(p =>
                    p.SystemType == SettingSystemType.OmsId)?.Value;
            _connectionID = settings
                .FirstOrDefault(p =>
                    p.SystemType == SettingSystemType.ConnectionId)?.Value;
            _crtificateThumbprint = settings
                .FirstOrDefault(p =>
                    p.SystemType == SettingSystemType.CertificateThumbprint)?.Value;
            _codesBlockQuantity = int.TryParse(settings
                .FirstOrDefault(p =>
                    p.SystemType == SettingSystemType.CodesBlockQuantity)?.Value, out int result) ? result : 1000;
            _delayMilliseconds = int.TryParse(settings
                .FirstOrDefault(p =>
                    p.SystemType == SettingSystemType.DelayMilliseconds)?.Value, out int delayMilliseconds) ? delayMilliseconds : 500;
            _iterationsCount = int.TryParse(settings
                .FirstOrDefault(p =>
                    p.SystemType == SettingSystemType.IterationsCount)?.Value, out int iterations) ? iterations : 50;
            _participantId = settings
                .FirstOrDefault(p =>
                    p.SystemType == SettingSystemType.ParticipantId)?.Value;
        }
    }
}
