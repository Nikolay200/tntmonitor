﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace TnTMonitor.SuzAPI.Infrastructure.ServiceData
{
    public enum CodeStatus
    {
        [Description("Код не использовался")]
        [JsonProperty("NEW")]
        New,
        [Description("Код находится в буфере принтера")]
        [JsonProperty("PRINTING")]
        Printing,
        [Description("Код напечатан")]
        [JsonProperty("PRINTED")]
        Printed,
        [Description("Код валидирован")]
        [JsonProperty("VALIDATED")]
        Validated,
    }
}
