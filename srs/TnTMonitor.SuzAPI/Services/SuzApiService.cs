﻿using FairMark.OmsApi;
using FairMark.OmsApi.DataContracts;
using FairMark.TrueApi.DataContracts;
using PlantX.DataContract.Services;
using PlantX.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TnTMonitor.DataContract.Dto.Aggregation;
using TnTMonitor.DataContract.Dto.Codes;
using TnTMonitor.DataContract.Dto.Dropout;
using TnTMonitor.DataContract.Dto.Logging;
using TnTMonitor.DataContract.Dto.Message;
using TnTMonitor.DataContract.Dto.Order;
using TnTMonitor.DataContract.Dto.Utilisation;
using TnTMonitor.DataContract.Services;
using TnTMonitor.MilkStore.Infrastructure.ServiceData;

namespace TnTMonitor.SuzAPI.Services
{
    public class SuzApiService : ISuzApiService
    {

        private AuthTokenServiceParameters _authenticateParameters;
        private CodesServiceParameters _codesParameters;
        private NumericalServiceParameters _numericalServiceParameters;
        private readonly ISuzApiSettings _suzApiSettings;        
        private readonly ILoggingService _loggingService;
        private readonly IMessageService _messagesService;
        private readonly IMilkStoreService _milkStoreService;        
        private OmsApiClient _omsApiClient;
        private OmsCredentials _omsCredentials;
        private static string path = @"AggregationInfo.json";
        public SuzApiService(
            ISuzApiSettings suzApiSettings,            
            ILoggingService loggingService,
            IMessageService messagesService,
            IMilkStoreService milkStoreService
            )
        {
            _suzApiSettings = suzApiSettings;            
            _loggingService = loggingService;
            _messagesService = messagesService;
            _milkStoreService = milkStoreService;
            Init();
        }

        /// <summary>
        /// Initialisation of parameters for SUZ-requests.
        /// Инициализация параметров для запросов в СУЗ.
        /// </summary>
        public void Init()
        {
            if (!_suzApiSettings.IsSettingsConfigured()) return;
            _authenticateParameters = new AuthTokenServiceParameters
            {
                CertificateThumbprint = _suzApiSettings.CertificateThumbprint(),
                ConnectionID = _suzApiSettings.ConnectionId(),
                OmsID = _suzApiSettings.OmsId(),
                UrlTokenRequest = _suzApiSettings.TrueApiServer(),
                UrlSuzServer = _suzApiSettings.SuzServer(),
                ParticipantId = _suzApiSettings.ParticipantId()
            };

            _codesParameters = new CodesServiceParameters
            {
                CodesBlockQuantity = _suzApiSettings.CodesBlockQuantity()
            };

            _omsCredentials = new OmsCredentials
            {
                OmsConnectionID = _authenticateParameters.ConnectionID,
                CertificateThumbprint = _authenticateParameters.CertificateThumbprint,
                OmsID = _authenticateParameters.OmsID,
            };

            _omsApiClient = new OmsApiClient(
                _authenticateParameters.UrlSuzServer,
                _authenticateParameters.UrlTokenRequest,
                ProductGroupsOMS.milk,
                _omsCredentials);

            _numericalServiceParameters = new NumericalServiceParameters
            {
                DelayMilliseconds = _suzApiSettings.DelayMilliseconds(),
                IterationsCount = _suzApiSettings.IterationsCount()
            };
        }

        /// <summary>
        /// Create Order.
        /// Создание заказа в СУЗ.
        /// </summary>

        public void CreateOrder(Guid orderId)
        {
            try
            {
                CheckStatus(orderId, OrderStatusDto.Draft, OrderStatusDto.CreatedSuz);
                Task.Factory.StartNew(() => CreateSuzOrder(orderId));
            }
            catch (Exception createOrderException)
            {
                AddLog($"Нельзя создать заказ, если он не в статусе {OrderStatusDto.Draft}. Пожалуйста, проверьте статус заказа.");
                _messagesService.AddMessage(new PlantX.DataContract.Dto.MessageDto
                {
                    CreateTime = DateTime.Now,
                    Text = $"{createOrderException.GetInnerMassagesOneString()}",
                    MessageType = PlantX.DataContract.Dto.MessageTypeDto.Alarm,
                    MessageGroup = MessageGroupDto.L3.ToString()//Необходимо добавить enum в PlantX.DataContract.Dto.MessageGroupDto
                });                
            }
        }
        private void CheckStatus(Guid orderId, OrderStatusDto correctStatus, OrderStatusDto nextStatus)
        {
            if (_milkStoreService.GetOrder(orderId).OrderStatus != correctStatus)
            {
                throw new Exception($"Статус должен быть {correctStatus}. Пожалуйста, проверьте статус.");
            }
            AddLog($"Статус для {orderId} изменён на {nextStatus}");
            _milkStoreService.SetOrderStatus(orderId, nextStatus);

        }
        public void CreateSuzOrder(Guid orderId)
        {
            try
            {
                OrderMilkDto orderMilk = _milkStoreService.GetOrder(orderId);
                var mappedOrderMilk = new Order_Milk
                {
                    ContactPerson = orderMilk.ContactPerson,
                    ProductionOrderID = orderMilk.OrderId.ToString(),
                    Products = _milkStoreService.GetProductsByOrderId(orderId)
                                        .Select(x => new OrderProduct_Milk
                                        {
                                            CisType = (CisTypes?)x.CisType,
                                            Gtin = x.Gtin,
                                            Quantity = x.Quantity,
                                            TemplateID = x.TemplateID,
                                            StickerID = x.StickerID,
                                            SerialNumberType = (SerialNumberTypes)x.SerialNumberType,
                                            SerialNumbers = null
                                        })
                                        .ToList(),
                    CreateMethodType = (CreateMethodTypes)orderMilk.CreateMethodType,
                    ReleaseMethodType = (ReleaseMethodTypes)orderMilk.ReleaseMethodType,
                    ServiceProviderID = orderMilk.CreateMethodType == CreateMethodTypesDto.SELF_MADE ? null : orderMilk.ServiceProviderID
                };
                AddLog($"Заказ {orderId} создаётся");

                var response = _omsApiClient.CreateOrder(mappedOrderMilk);
                orderMilk.SuzOrderId = response.OrderID;
                _milkStoreService.SaveOrder(orderMilk);

                AddLog($"Заказ {orderId} подписан и отправлен на подтверждение в ГИС МТ");
                Thread.Sleep((int)response.ExpectedCompleteTimestamp);
                SuzOrderRequest(orderMilk);
            }
            catch (Exception ex)
            {
                SetErrorStatus(orderId, OrderStatusDto.OrderCreateError, $"Заказ {orderId} не может быть создан. {ex.GetInnerMassagesOneString()}");
            }
        }
        private void SuzOrderRequest(OrderMilkDto order)
        {
            int i = 0;
            while (_numericalServiceParameters.IterationsCount >= i)
            {
                i++;
                var orderStatus = GetOrderStatusFromSuz(order.SuzOrderId);

                if (orderStatus == OrderStatuses.EXPIRED || orderStatus == OrderStatuses.DECLINED)
                {
                    SetErrorStatus(order.OrderId, OrderStatusDto.OrderCreateError, $"Заказ {order.OrderId} не может быть создан.");
                    break;
                }

                if (orderStatus != OrderStatuses.READY)
                {
                    _milkStoreService.SetOrderStatus(order.OrderId, OrderStatusDto.CreatedSuz);
                    Thread.Sleep(500);
                    continue;
                }

                _milkStoreService.SetOrderStatus(order.OrderId, OrderStatusDto.EnabledSuz);
                AddLog($"Заказ {order.OrderId} успешно создан.");
                break;
            }
        }
        public OrderStatuses GetOrderStatusFromSuz(Guid? suzOrderId)
        {
            return _omsApiClient.GetOrders().OrderInfos.FirstOrDefault(x => x.OrderID == suzOrderId)?.OrderStatus ?? throw new Exception($"Заказ с ID: {suzOrderId} не найден.");
        }
        private void AddLog(string message, LogTypeDto logType = LogTypeDto.Info)
        {            
            _loggingService.AddLogItem(message, LogSources.HttpRequest.ToString(), logType);//Необходимо добавить enum в PlantX.DataContract.Dto.LogSourcesDto
        }

        /// <summary>
        /// Load Codes.
        /// Загрузить КМ в базу данных из СУЗ.
        /// </summary>

        public void LoadCodes(Guid orderId)
        {
            try
            {
                CheckStatus(orderId, OrderStatusDto.EnabledSuz, OrderStatusDto.ActiveCodesSuz);
                Task.Factory.StartNew(() => GetCodesFromSuz(orderId));
            }
            catch (Exception loadCodesException)
            {
                AddLog($"Нельзя начать загрузку кодов, если заказ не в статусе {OrderStatus.EnabledSuz}. Пожалуйста, проверьте статус заказа.");
                _messagesService.AddMessage(new PlantX.DataContract.Dto.MessageDto
                {
                    CreateTime = DateTime.Now,
                    Text = $"{loadCodesException.GetInnerMassagesOneString()}",
                    MessageType = PlantX.DataContract.Dto.MessageTypeDto.Alarm,
                    MessageGroup = MessageGroupDto.L3.ToString()//Необходимо добавить enum в PlantX.DataContract.Dto.MessageGroupDto
                });
            }
        }
        private void GetCodesFromSuz(Guid orderId)
        {
            try
            {
                var order = _milkStoreService.GetOrder(orderId);
                if (order == null)
                {
                    AddLog($"Заказа {orderId} нет в базе данных.");
                    _messagesService.AddMessage(new PlantX.DataContract.Dto.MessageDto
                    {
                        CreateTime = DateTime.Now,
                        Text = $"Заказ с номером {orderId} не найден.",
                        MessageType = PlantX.DataContract.Dto.MessageTypeDto.Alarm,
                        MessageGroup = MessageGroupDto.L3.ToString() //Необходимо добавить enum в PlantX.DataContract.Dto.MessageGroupDto
                    });                    
                    SetErrorStatus(orderId, OrderStatusDto.CodeLoadError, $"Заказ {orderId} не найден.");
                    return;
                }

                AddLog($"Начинается загрузка кодов для заказа {orderId}.");
                var isDisabled = false;
                foreach (var product in order.Products)
                {
                    AddLog($"Начинается загрузка кодов для продукта {product.Gtin} с ID: {product.Id}.");
                    var status = GetCodesBufferStatusFromSuz(order.SuzOrderId, product);

                    if (status == BufferStatuses.EXPIRED || status == BufferStatuses.REJECTED)
                    {
                        AddLog($"Коды для продукта {product.Gtin} с ID: {product.Id} не доступны. Ошибка во время загрузки.");
                        isDisabled = true;
                        continue;
                    }

                    var codesDto = _omsApiClient.GetCodes(order.SuzOrderId.ToString(), product.Gtin, Math.Min(product.Quantity, _codesParameters.CodesBlockQuantity));
                    _milkStoreService.SaveCodes(codesDto.Codes.Select(p => new CodeItemDto { Code = p, OrderId = orderId, OrderProductId = product.Id, CodeStatus = CodeStatusDto.New, CodeType = product.CisType }).ToList());
                    var lastBlockId = codesDto.BlockID;
                    var availableCodes = product.Quantity;

                    while (availableCodes > 0)
                    {
                        availableCodes = GetAvailableCodes(order.SuzOrderId, product);

                        if (availableCodes != 0)
                        {
                            codesDto = _omsApiClient.GetCodes(order.SuzOrderId.ToString(), product.Gtin, Math.Min(availableCodes, _codesParameters.CodesBlockQuantity), lastBlockId);
                            _milkStoreService.SaveCodes(codesDto.Codes.Select(p => new CodeItemDto { Code = p, OrderId = orderId, OrderProductId = product.Id, CodeStatus = CodeStatusDto.New, CodeType = product.CisType }).ToList());
                            lastBlockId = codesDto.BlockID;
                            continue;
                        }
                    }
                    AddLog($"Коды для продукта {product.Gtin} с ID: {product.Id} успешно загружены.");
                }
                if (isDisabled)
                {
                    SetErrorStatus(orderId, OrderStatusDto.CodeLoadError, $"Коды для заказа {orderId} не доступны. Ошибка во время загрузки.");
                }
                else
                {
                    AddLog($"Коды для заказа {orderId} успешно загружены.");
                    _milkStoreService.SetOrderStatus(orderId, OrderStatusDto.Ready);
                }
            }

            catch (Exception ex)
            {
                SetErrorStatus(orderId, OrderStatusDto.CodeLoadError, $"Коды для заказа {orderId} не доступны. Ошибка во время загрузки, {ex.GetInnerMassagesOneString()}");
            }
        }

        private void SetErrorStatus(Guid orderId, OrderStatusDto orderStatus, string message)
        {
            _milkStoreService.SetOrderStatus(orderId, orderStatus);
            AddLog(message);
            _messagesService.AddMessage(new PlantX.DataContract.Dto.MessageDto
            {
                CreateTime = DateTime.Now,
                Text = message,
                MessageType = PlantX.DataContract.Dto.MessageTypeDto.Alarm,
                MessageGroup = MessageGroupDto.L3.ToString() //Необходимо добавить enum в PlantX.DataContract.Dto.MessageGroupDto
            });            
        }

        private int GetAvailableCodes(Guid? orderId, OrderProductMilkDto product)
        {
            return _omsApiClient.GetBufferStatus(orderId.ToString(), product.Gtin).AvailableCodes;
        }

        private BufferStatuses GetCodesBufferStatusFromSuz(Guid? orderId, OrderProductMilkDto product)
        {
            return _omsApiClient.GetBufferStatus(orderId.ToString(), product.Gtin).BufferStatus;
        }

        /// <summary>
        /// Send Reports.
        /// Отправка отчётов в СУЗ (отчёт об использовании, отчёт об агрегации и отчёт о выбытии/отбраковке КМ).
        /// </summary>
        public void SendReports(Guid orderId) => Task.Factory.StartNew(() =>
        {
            AddLog($"Sending Utilization Reports");
            _milkStoreService.SetOrderStatus(orderId, OrderStatusDto.SendingReports);

            _milkStoreService.GetUtilisationReportData(orderId)
                .PlantXMap<List<UtilisationReportDto>, List <UtilisationReport>>()
                .ForEach(p => _omsApiClient.Utilisation(p));
            AddLog($"Sending Utilization Reports complete");

        })
        .ContinueWith(t =>
        {
            if (t.IsFaulted)
            {
                AddLog($"Exception while sending Utilisation Report. Message = {t.Exception.GetInnerMassagesOneString()}", LogTypeDto.Critical);                
                _messagesService.AddMessage(new PlantX.DataContract.Dto.MessageDto
                {
                    CreateTime = DateTime.Now,
                    Text = "Ошибки при отправке отчетов по утилизации",
                    MessageType = PlantX.DataContract.Dto.MessageTypeDto.Alarm,
                    MessageGroup = MessageGroupDto.L3.ToString() //Необходимо добавить enum в PlantX.DataContract.Dto.MessageGroupDto
                });
                _milkStoreService.SetOrderStatus(orderId, OrderStatusDto.ReportSendingError);
            }
        })
        .ContinueWith(t2 =>
        {
            AddLog($"Sending Aggregation Reports");
            _milkStoreService.GetAggregationReportData(orderId)
                .PlantXMap<List<AggregationReportDto>, List<AggregationReport>>()
                .ForEach(p => _omsApiClient.CreateAggregation(p));
            AddLog($"Sending Aggregation Reports complete");
        })
        .ContinueWith(t3 =>
        {
            if (t3.IsFaulted)
            {
                AddLog($"Exception while sending Aggregation Report. Message = {t3.Exception.GetInnerMassagesOneString()}", LogTypeDto.Critical);                
                _messagesService.AddMessage(new PlantX.DataContract.Dto.MessageDto
                {
                    CreateTime = DateTime.Now,
                    Text = "Ошибки при отправке отчетов по агрегации",
                    MessageType = PlantX.DataContract.Dto.MessageTypeDto.Alarm,
                    MessageGroup = MessageGroupDto.L3.ToString() //Необходимо добавить enum в PlantX.DataContract.Dto.MessageGroupDto
                });
                _milkStoreService.SetOrderStatus(orderId, OrderStatusDto.ReportSendingError);
            }
        })
        .ContinueWith(t4 =>
        {
            AddLog($"Sending Dropout Reports");
            _milkStoreService.GetDropoutReportData(orderId)
                .PlantXMap<List<DropoutReportDto>, List<DropoutReport>>()
                .ForEach(p => _omsApiClient.Dropout(p));
            AddLog($"Sending Dropout Reports complete");
            _milkStoreService.SetOrderStatus(orderId, OrderStatusDto.ReportsDelivered);
        })
        .ContinueWith(t5 =>
        {
            if (t5.IsFaulted)
            {
                AddLog($"Exception while sending Dropout Report. Message = {t5.Exception.GetInnerMassagesOneString()}", LogTypeDto.Critical);                
                _messagesService.AddMessage(new PlantX.DataContract.Dto.MessageDto
                {
                    CreateTime = DateTime.Now,
                    Text = "Ошибки при отправке отчетов о выбытии/отбраковке КМ",
                    MessageType = PlantX.DataContract.Dto.MessageTypeDto.Alarm,
                    MessageGroup = MessageGroupDto.L3.ToString() //Необходимо добавить enum в PlantX.DataContract.Dto.MessageGroupDto
                });
                _milkStoreService.SetOrderStatus(orderId, OrderStatusDto.ReportSendingError);
            }
        });


        /// <summary>
        /// Synchronization.
        /// Синхронизация с заказами в СУЗ.
        /// </summary>

        public void SynchronizeWithSuz()
        {
            try
            {

                _milkStoreService.GetAllOrders().Synchronizer(_omsApiClient.GetOrders().OrderInfos.ToList()
                    , (suzOrder, pagOrder) => suzOrder.OrderStatus == pagOrder.SuzOrderStatus.PlantXMap<OrderStatuses, OrderStatusesDto>()
                                              && suzOrder.Buffers.Count == pagOrder.Products.Count
                                              && suzOrder.ProductionOrderId == pagOrder.ProductionOrderID
                                              && suzOrder.DeclineReason == pagOrder.DeclineReason
                                              && suzOrder.Buffers.All(p => CompareProduct(p, pagOrder.Products.PlantXMap<List<OrderProductMilk>, List<OrderProductMilkDto>>().FirstOrDefault(pp => p.Gtin == pp.Gtin), GetCardProduct(p))),
                    (suzOrder, pagOrder) =>
                    {
                        pagOrder.SuzOrderStatus = suzOrder.OrderStatus.PlantXMap<OrderStatusesDto, OrderStatuses>();
                        pagOrder.CreateTime = TimeStampToDateTime(suzOrder.CreatedTimestamp);
                        pagOrder.DeclineReason = suzOrder.DeclineReason;
                        var cashProducts = GetCardsProductsByOrderId(suzOrder.OrderID);
                        pagOrder.Products.Synchronizer(suzOrder.Buffers, (suzProduct, pagProduct) => CompareProduct(suzProduct,
                                pagProduct.PlantXMap<OrderProductMilk, OrderProductMilkDto>(),
                                cashProducts.FirstOrDefault(x => suzProduct.Gtin == x.Gtin)),
                            (suzProduct, pagProduct) =>
                            {
                                pagProduct.RejectionReason = suzProduct.RejectionReason;
                                pagProduct.BufferStatus = suzProduct.BufferStatus.PlantXMap<BufferStatuses, BufferStatusesDto>();
                                pagProduct.Quantity = suzProduct.TotalCodes;
                                pagProduct.Gtin = suzProduct.Gtin;
                                pagProduct.ExpDate = TimeStampToDateTime(suzProduct.ExpiredDate);
                                pagProduct.ExpDate72 = TimeStampToDateTime(suzProduct.ExpiredDate);

                                var cardProduct = cashProducts.FirstOrDefault(x => suzProduct.Gtin == x.Gtin);
                                pagProduct.Fat = double.Parse(cardProduct.Fat);
                                pagProduct.Weight = double.Parse(cardProduct.VolumeWeight.TrimEnd('г')) / 1000;
                                pagProduct.ProductName = cardProduct.Name;

                            }, suzProduct => pagOrder.Products.Add(CreateNewPagProductBySuzOrder(suzProduct, pagOrder.OrderId, cashProducts.FirstOrDefault(x => x.Gtin == suzProduct.Gtin)))
                            , pagProduct => pagOrder.Products.RemoveAll(p => p.Gtin == pagProduct.Gtin));
                        _milkStoreService.SaveOrder(pagOrder);
                    }, suzOrder =>
                    {
                        var newOrder = CreateNewPagOrderBySuzOrder(suzOrder);
                        newOrder.Products = suzOrder.Buffers.Select(p => CreateNewPagProductBySuzOrder(p, newOrder.OrderId, GetCardProduct(p)))
                            .ToList();
                        _milkStoreService.SaveOrder(newOrder);
                    }, pagOrder => _milkStoreService.FullRemoveOrder(pagOrder.OrderId));

                AddLog("Данные по заказам синхронизированы.");
            }
            catch (Exception e)
            {
                AddLog($"Ошибка при синхронизации заказов. {e.GetInnerMassagesOneString()}");
                throw;
            }
        }

        private bool CompareProduct(BufferInfo suzProduct, OrderProductMilk pagProduct, ProductsInfo card)
        {
            return suzProduct.TotalCodes == pagProduct.Quantity
                   && suzProduct.RejectionReason == pagProduct.RejectionReason
                   && suzProduct.BufferStatus == pagProduct.BufferStatus
                   && suzProduct.Gtin == pagProduct.Gtin
                   && (TimeStampToDateTime(suzProduct.ExpiredDate) == pagProduct.ExpDate || TimeStampToDateTime(suzProduct.ExpiredDate) == pagProduct.ExpDate72)
                   && double.Parse(card.Fat) == pagProduct.Fat
                   && card.Name == pagProduct.ProductName
                   && double.Parse(card.VolumeWeight.TrimEnd('г')) / 1000 == pagProduct.Weight;
        }
        private ProductsInfo GetCardProduct(BufferInfo suzProduct)
        {
            return _omsApiClient.GetCardsInfo(suzProduct.OrderID.ToString()).FirstOrDefault(x => x.Key == suzProduct.Gtin).Value;
        }
        private List<ProductsInfo> GetCardsProductsByOrderId(Guid orderId)
        {
            return _omsApiClient.GetCardsInfo(orderId.ToString()).
                Select(x => new ProductsInfo
                {
                    Gtin = x.Key,
                    Name = x.Value.Name,
                    Fat = x.Value.Fat,
                    VolumeWeight = x.Value.VolumeWeight
                }).ToList();
        }
        private OrderMilkDto CreateNewPagOrderBySuzOrder(OrderSummaryInfo suzOrder)
        {
            return new OrderMilkDto()
            {
                SuzOrderId = suzOrder.OrderID,
                ProductionOrderID = suzOrder.ProductionOrderId == null ? new Random().Next(1000).ToString() : suzOrder.ProductionOrderId,
                SuzOrderStatus = (OrderStatusesDto)suzOrder.OrderStatus,
                DeclineReason = suzOrder.DeclineReason,
                CreateTime = TimeStampToDateTime(suzOrder.CreatedTimestamp)
            };
        }
        private OrderProductMilkDto CreateNewPagProductBySuzOrder(BufferInfo suzProduct, Guid orderId, ProductsInfo card)
        {
            return new OrderProductMilkDto
            {
                RejectionReason = suzProduct.RejectionReason,
                Quantity = suzProduct.TotalCodes,
                BufferStatus = suzProduct.BufferStatus.PlantXMap<BufferStatuses, BufferStatusesDto>(),
                OrderId = orderId,
                Gtin = suzProduct.Gtin,
                ExpDate = TimeStampToDateTime(suzProduct.ExpiredDate),
                ExpDate72 = TimeStampToDateTime(suzProduct.ExpiredDate),
                Weight = double.Parse(card.VolumeWeight.TrimEnd('г')) / 1000,
                Fat = double.Parse(card.Fat),
                ProductName = card.Name
            };
        }

        public static DateTime TimeStampToDateTime(long createdTimeStamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddMilliseconds(createdTimeStamp).ToLocalTime();
            return dateTime;
        }
    }
    public class AuthTokenServiceParameters
    {
        /// <summary>
        /// Отпечаток. 40-значный код в сертификате.
        /// </summary>
        public string CertificateThumbprint { get; set; }
        /// <summary>
        /// Адрес сервера аутентификации.
        /// </summary>
        public string UrlTokenRequest { get; set; }
        /// <summary>
        /// Идентификатор соединения с СУЗ.
        /// </summary>
        public string ConnectionID { get; set; }
        /// <summary>
        /// Идентификатор пользователя СУЗ.
        /// </summary>
        public string OmsID { get; set; }
        /// <summary>
        /// Адрес сервера СУЗ.
        /// </summary>
        public string UrlSuzServer { get; set; }
        /// <summary>
        /// Идентификационный номер(ИНН) налогоплательщика.
        /// </summary>
        public string ParticipantId { get; set; }
    }
    public class CodesServiceParameters
    {
        /// <summary>
        /// Количество кодов в блоке.
        /// </summary>
        public int CodesBlockQuantity { get; set; }
    }
    public class NumericalServiceParameters
    {
        /// <summary>
        /// Количество миллисекунд.
        /// </summary>
        public int DelayMilliseconds { get; set; }
        /// <summary>
        /// Количество итераций в цикле.
        /// </summary>
        public int IterationsCount { get; set; }
    }
}
